package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.model.RequestName;
import demo.model.Response;
import demo.service.DemoService;

@RestController
public class DemoController {
	@Autowired
	private DemoService demoService;

	@GetMapping("/index")
	public String home() {
		return "this is demo application";
	}

	@PostMapping("/name")
	public ResponseEntity<Response> addName(@RequestBody RequestName request) {

		demoService.addName(request);
		Response response = new Response();
		response.setMessage("my request success");
		response.setStatusCode(200);
		return ResponseEntity.ok(response);
	}

	@GetMapping("/getname/{id}")
	public String getName(@PathVariable int id) {
		String name = demoService.getName(id);
		return name;
		

	}
	@GetMapping("/getnamebyparam/{id}")
	public String getNameByParam(@PathVariable int id,@RequestParam(name="welcome",required=false,defaultValue="notwelcome") String name,Model model) {
		model.addAttribute("welcome",name);
		String myname = demoService.getName(id);
		
		return name+" "+myname;
		

	}

}
