package demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.dao.DemoRepository;
import demo.model.RequestName;

@Service
public class DemoService {
	@Autowired
	private DemoRepository demo;
	
	public void addName(RequestName request) {
		demo.save(request);
		
	}
	public String getName(int id) {
		Optional <RequestName> request=demo.findById(id);
		String name=request.get().getName();
		return name;
	}

}
