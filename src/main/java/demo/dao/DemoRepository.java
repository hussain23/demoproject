package demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import demo.model.RequestName;

@Repository
public interface DemoRepository extends CrudRepository<RequestName,Integer>

{

}
